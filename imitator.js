var calculatorPackage = require("./calculator");
var calc = calculatorPackage.calculator;
var _ = require("underscore");

var createEmpiricalProbabilities = function () {
	var min, max;

	var result = Object.create(calculatorPackage.distributionBase);

	//increment count at index
	result.increment = function (index) {
		this[index] = this.at(index) + 1;
		if (min === undefined)
			min = index;
		else if (index < min)
			min = index;

		if (max === undefined)
			max = index;
		else if (max < index)
			max = index;
	};

	//convert counts to probabilities
	result.normalize = function () {
		var sum = 0;
		for (var i = min; i <= max; ++i)
			sum += this.at(i);
		if (sum != 0)
			for (var i = min; i <= max; ++i)
				this[i] = this.at(i) / sum;
	};

	result.min = function() {
		return Math.min(0, min);
	};

	result.max = function() {
		return Math.max(max, 0);
	};

	return result;
};

var Imitator = function () {
	var im = this;

	this.imitationIterations = 10000;

	this.imitateBattle = function (attackerFull, defenderFull, battleType, options) {
		options = options || {attacker:{},defender:{}};
		var attacker = _.filter(attackerFull, function (unit) { return calc.belongsToBattle(unit, battleType, options.attacker.gravitonNegator); });
		var defender = _.filter(defenderFull, function (unit) { return calc.belongsToBattle(unit, battleType); });

		for (var i = 0; i < prebattleActions.length; i++) {
			var action = prebattleActions[i];
			if (action.appliesTo === battleType)
				action.execute(attacker, defender, attackerFull, defenderFull, options);
		}
		var round = 0;
		while (hasUnits(attacker) && hasUnits(defender)) {
			round++;
			var attackerInflicted = rollDice(attacker, options.attacker.moraleBoost1 && round === 1? 1: 0);
			var defenderInflicted = rollDice(defender, options.defender.moraleBoost1 && round === 1? 1: 0);
			
			if ( round > 30){
				// deadlock detected, defender wins
				attackerInflicted = 0;
				defenderInflicted = attacker.length;
			}
							
			applyDamage(attacker, defenderInflicted, options.attacker.duraniumArmor);
			applyDamage(defender, attackerInflicted, options.defender.duraniumArmor);
		}

		return { attacker: attacker, defender: defender };
	};

	this.estimateProbabilities = function (attacker, defender, battleType, options) {
		options = options || {attacker:{},defender:{}};
		var result = createEmpiricalProbabilities();
		for (var i = 0; i < im.imitationIterations; ++i) {
			var tmpAttacker = attacker.map(function (unit) { return unit.clone(); });
			var tmpDefender = defender.map(function (unit) { return unit.clone(); });

			var survivors = im.imitateBattle(tmpAttacker, tmpDefender, battleType, options);

			if (survivors.attacker.length !== 0)
				result.increment(-survivors.attacker.length);
			else if (survivors.defender.length !== 0)
				result.increment(survivors.defender.length);
			else
				result.increment(0);
		}
		result.normalize();

		return result;
	};

	var applyDamage = function (fleet, hits, duraniumArmor) {
		//for (var i = 0; i < fleet.length && hits > 0; i++) {
		//	if (fleet[i].isDamageable && !fleet[i].isDamaged) {
		//		fleet[i].isDamaged = true;
		//		hits--;
		//	}
		//}
			
		for (var i = 0; i < hits; i++)
			fleet.pop();
		
		if (duraniumArmor && hits > 0){
			var fleetLength = fleet.length;
			for ( var i = 0; i < fleetLength; i++){
				if (fleet[i].isDamageable){
					var unitType = fleet[i].type;
					fleet.push( calc.units[unitType].toDamageGhost());
					break;
				}
			}
		}
	};

	var rollDice = function (fleet, boost, reroll) {
		boost = boost || 0;
		var totalRoll = 0;
		for (var i = 0; i < fleet.length; i++) {
			var unit = fleet[i];
			for (var die = 0; die < unit.diceRolled; ++die)
				if (unit.dmgDice <= rollDie() + boost
					|| reroll && (unit.dmgDice <= rollDie() + boost))
					totalRoll++;
		}
		return totalRoll;
	};

	var rollDie = function () {
		return Math.floor(Math.random() * calc.dieSides() + 1);
	};

	var hasUnits = function (fleet) {
		//return _.any(fleet, function (unit) { return belongsToBattle(unit, battleType); });
		return fleet.length > 0;
	};

	var unitIs = function(unitType) {
		return function(unit) {
			return unit.type === unitType;
		};
	};

	var prebattleActions = [
		{
			name: "pds -> ships",
			appliesTo: calc.BattleType.Space,
			execute: function (attacker, defender, attackerFull, defenderFull, options) {
				var attackerInflicted = rollDice(_.filter(attackerFull, unitIs(calc.UnitType.PDS)), 0, options.attacker.gravitonLaser);
				var defenderInflicted = rollDice(_.filter(defenderFull, unitIs(calc.UnitType.PDS)), 0, options.defender.gravitonLaser);
				applyDamage(attacker, defenderInflicted);
				applyDamage(defender, attackerInflicted);
			}
		},
		{
			name: "mentak racial",
			appliesTo: calc.BattleType.Space,
			execute: function (attacker, defender, attackerFull, defenderFull, options) {

				var getInflicted = function(fleet){
					var firing = _.filter(fleet, unitIs(calc.UnitType.Cruiser));
					if (firing.length < 2)
						firing = firing.concat(_.filter(fleet, unitIs(calc.UnitType.Destroyer)));
					if (firing.length > 2)
						firing = firing.slice(0,2);
					return rollDice(firing);
				};
				var attackerInflicted = 0;
				var defenderInflicted = 0;
				if (options.attacker.mentak)
					attackerInflicted = getInflicted(attacker);
				if (options.defender.mentak)
					defenderInflicted = getInflicted(defender);
				applyDamage(attacker, defenderInflicted);
				applyDamage(defender, attackerInflicted);
			}
		},
		{
			name: "assault cannon",
			appliesTo: calc.BattleType.Space,
			execute: function (attacker, defender, attackerFull, defenderFull, options) {

				var attackerInflicted = options.attacker.assaultCannon ? rollDice(_.filter(attacker, unitIs(calc.UnitType.Dreadnought))) : 0;
				var defenderInflicted = options.defender.assaultCannon ? rollDice(_.filter(defender, unitIs(calc.UnitType.Dreadnought))) : 0;
				applyDamage(attacker, defenderInflicted);
				applyDamage(defender, attackerInflicted);
			}
		},
		{
			name: "anti-fighter barrage",
			appliesTo: calc.BattleType.Space,
			execute: function (attacker, defender, attackerFull, defenderFull, options) {
				var attackerDestroyers = _.filter(attacker, unitIs(calc.UnitType.Destroyer));
				var defenderDestroyers = _.filter(defender, unitIs(calc.UnitType.Destroyer));
				//each destroyer rolls two dice (three with Defence Turret tech). NB! rollDice returns random results
				var attackerInflicted = rollDice(attackerDestroyers) + rollDice(attackerDestroyers) + (options.attacker.defenceTurret ? rollDice(attackerDestroyers): 0);
				var defenderInflicted = rollDice(defenderDestroyers) + rollDice(defenderDestroyers) + (options.defender.defenceTurret ? rollDice(attackerDestroyers): 0);
				for (var i = attacker.length - 1; 0 <= i && 0 < defenderInflicted; i--) {
					if (attacker[i].type === calc.UnitType.Fighter) {
						attacker.splice(i, 1);
						defenderInflicted--;
					}
				}
				for (var i = defender.length - 1; 0 <= i && 0 < attackerInflicted; i--) {
					if (defender[i].type === calc.UnitType.Fighter) {
						defender.splice(i, 1);
						attackerInflicted--;
					}
				}
			}
		},
		{
			name: "pds -> ground forces",
			appliesTo: calc.BattleType.Ground,
			execute: function (attacker, defender, attackerFull, defenderFull, options) {
				var defenderInflicted = rollDice(_.filter(defenderFull, unitIs(calc.UnitType.PDS)), 0, options.defender.gravitonLaser);

				for (var i = attacker.length - 1; 0 <= i && 0 < defenderInflicted; i--) {
					if (attacker[i].type === calc.UnitType.Ground) {
						attacker.splice(i, 1);
						defenderInflicted--;
					}
				}
			}
		},
		{
			name: "WarSun bombardment",
			appliesTo: calc.BattleType.Ground,
			execute: function (attacker, defender, attackerFull, defenderFull) {
				var attackerInflicted = rollDice(_.filter(attackerFull, unitIs(calc.UnitType.WarSun)));

				for (var i = defender.length - 1; 0 <= i && 0 < attackerInflicted; i--) {
					if (defender[i].type === calc.UnitType.Ground) {
						defender.splice(i, 1);
						attackerInflicted--;
					}
				}
			}
		},
		{
			name: "Dreadnought bombardment",
			appliesTo: calc.BattleType.Ground,
			execute: function (attacker, defender, attackerFull, defenderFull, options) {

				if (!_.any(attackerFull, unitIs(calc.UnitType.Dreadnought))) return; //if no dreadnaughts no bombardment
				if (!_.any(attackerFull, unitIs(calc.UnitType.Ground)) && !_.any(attackerFull, unitIs(calc.UnitType.Mech))) return  //if no ground forces & no mechs no bombardment
				if (!_.any(defenderFull, unitIs(calc.UnitType.Ground))) return; //if no defending ground forces no bombardment as mechs immune
				if (_.any(defenderFull, unitIs(calc.UnitType.PDS)) && !options.attacker.gravitonNegator) return; //dreadnoughts do not bombard over PDS. unless Graviton Negator

				var attackerInflicted = rollDice(_.filter(attackerFull, unitIs(calc.UnitType.Dreadnought)));

				for (var i = defender.length - 1; 0 <= i && 0 < attackerInflicted; i--) {
					if (defender[i].type === calc.UnitType.Ground) {
						defender.splice(i, 1);
						attackerInflicted--;
					}
				}
			}
		}
	];
};


//----- EXPORTS ----
if (!exports)
	exports = {};

exports.imitator = new Imitator();
exports.createEmpiricalProbabilities = createEmpiricalProbabilities;